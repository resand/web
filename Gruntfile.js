module.exports = function (grunt) {
    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);
    // Project configuration.  
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        cssmin: {
            sitecss: {
                options: {
                    banner: '/* My minified css file */'
                },
                files: {
                    'css/all.css': [
                        'css/*.css'
                    ]
                }
            }
        },
        uglify: {
            options: {
                compress: true
            },
            applib: {
                src: [
                    'js/jquery.js',
                    'js/typed.js',
                    'js/magnific-popup.js',
                    'js/masonry.pkgd.js',
                    'js/imagesloaded.pkgd.js',
                    'js/masonry-filter.js',
                    'js/scripts.js'
                ],
                dest: 'js/all.js'
            }
        }
    });
    // Default task.  
    grunt.registerTask('default', ['uglify', 'cssmin']);
};