/**
*   Glitche (HTML)
*   Copyright © Glitche by beshleyua. All Rights Reserved.
**/

$(function () {
    'use strict';

    $(window).unload(function () { });

    /* Set full height in blocks */
    var width = $(window).width();
    var height = $(window).height();
    $('.section.started').css({ 'height': height - 60 });

    /* Typed preload text */
    $('.typed-load').typed({
        stringsElement: $('.typing-load'),
        loop: true
    });

    /* Preloader */
    $(window).load(function () {
        for (var i = 0; i <= 6; i++) {
            $('#post-' + i).hide();
        }

        $(".preloader .pre-inner").fadeOut(800, function () {
            /* Preload hide */
            $('.preloader').fadeOut();
            $('body').addClass('loaded');

            /* Typed subtitle */
            $('.typed-subtitle').typed({
                stringsElement: $('.typing-subtitle'),
                loop: true
            });

            /* Typed breadcrumbs */
            $('.typed-bread').typed({
                stringsElement: $('.typing-bread'),
                showCursor: false
            });
        });

        /* Get Medium posts */
        $.ajax({
            type: 'GET',
            url: "https://api.rss2json.com/v1/api.json?rss_url=https://medium.com/feed/@resand",
            dataType: 'json',
            success: function (response) {
                if (response.status == "ok") {
                    if (response.items.length == 0) {
                        $('#link-block').remove();
                        $('#section-blog').remove();
                    }
                    var index = 0;
                    $.each(response.items, function (i, post) {
                        index++;
                        setBoxItem(post, index);

                        if (index == 6) return;
                    });
                }
            },
            error: function (error) {
                console.log("Occurrio un detalle al cargar los posts, intentaremos más tarde.");
            },
        });
    });

    function setBoxItem(post, index) {
        console.log(post);
        var date = new Date(post.pubDate);
        var image = post.thumbnail.includes('cdn-images') ? post.thumbnail : 'images/placeholder-image.png';
        //let months = ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sept", "oct", "nov", "dic"];
        //let formattedDate = date.getDate() + "-" + months[date.getMonth()] + "-" + date.getFullYear()
        var formattedHuman = timeSince(date);

        $('#post-' + index + ' .image img').attr('src', image);
        $('#post-' + index + ' .image a').attr('href', post.link);
        $('#post-' + index + ' .date').append(formattedHuman);
        $('#post-' + index + ' .name').append(post.title);
        $('#post-' + index + ' .name').attr('href', post.link);

        $('#post-' + index).show();
    }

    function timeSince(date) {
        var seconds = Math.floor((new Date() - date) / 1000);
        var interval = Math.floor(seconds / 31536000);
        var text = "Hace ";

        if (interval > 1) {
            return text + interval + " años";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return text + interval + " mesess";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return text + interval + " días";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return text + interval + " horas";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return text + interval + " minutos";
        }

        return text + Math.floor(seconds) + " segundos";
    }

    /* Menu mobile */
    $('header').on('click', '.menu-btn', function () {
        if ($('header').hasClass('active')) {
            $('header').removeClass('active');
            $('body').addClass('loaded');
        } else {
            $('header').addClass('active');
            $('body').removeClass('loaded');
        }

        return false;
    });

    /* Hide mouse button on scroll */
    $(window).scroll(function () {
        if ($(this).scrollTop() >= 1 /*$('#blue_bor').offset().top*/) {
            $('.mouse_btn').fadeOut();
        }
        else {
            $('.mouse_btn').fadeIn();
        }
    });

    $('body').on({
        mouseenter: function () {
            $(this).addClass('glitch-effect-white');
        },
        mouseleave: function () {
            $(this).removeClass('glitch-effect-white');
            $('.top-menu ul li.active a.btn').addClass('glitch-effect-white');
        }
    }, 'a.btn, .btn');

    $('.top-menu > ul > li > a').click(function (e) {
        e.preventDefault();
        var curLink = $(this);
        var scrollPoint = $(curLink.attr('href')).position().top - 100;
        $('body, html').animate({
            scrollTop: scrollPoint
        }, 500);
    })
    $(window).scroll(function () {
        onScrollHandle();
    });

    function onScrollHandle() {
        //Get current scroll position
        var currentScrollPos = $(document).scrollTop() + 120;
        //Iterate through all node
        $('.top-menu > ul > li > a').each(function () {
            var curLink = $(this);
            var refElem = $(curLink.attr('href'));
            //Compare the value of current position and the every section position in each scroll
            if (refElem.position().top <= currentScrollPos && refElem.position().top + refElem.height() > currentScrollPos) {
                //Remove class active in all nav
                $('.top-menu > ul > li').removeClass("active");
                //Add class active
                curLink.parent().addClass("active");
            }
            else {
                curLink.parent().removeClass("active");
            }
        });
        history.pushState('', document.title, window.location.pathname);
    }

    /* Initialize masonry items */
    var $container = $('.box-items');

    $container.imagesLoaded(function () {
        $container.multipleFilterMasonry({
            itemSelector: '.box-item',
            filtersGroupSelector: '.filters',
            percentPosition: true,
            gutter: 0
        });
    });

    /* Initialize masonry filter */
    $('.filters label').on('change', 'input[type="checkbox"]', function () {
        if ($(this).is(':checked')) {
            $(this).parent().addClass('glitch-effect');
        }
        else {
            $(this).parent().removeClass('glitch-effect');
        }
        /* Refresh Portfolio magnific popup */
        $('.has-popup').magnificPopup({
            type: 'inline',
            overflowY: 'auto',
            closeBtnInside: true,
            mainClass: 'mfp-fade'
        });
    });

    /* Resize function */
    $(window).resize(function () {
        var width = $(window).width();
        var height = $(window).height();

        $('.section.started').css({ 'height': height - 60 });
    });

    if (width < 840) {
        $('.section.started').css({ 'height': height - 30 });
    }

    /* Set copy */
    var x = document.getElementById("text-copy");
    var year = new Date().getFullYear();

    x.innerText = "© " + year + " René Sandoval. All rights reserved.";
});